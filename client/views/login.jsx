import React, { Component } from 'react';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';

export default class LoginPage extends Component{
  render(){
    return(
      <div style={styles.container}>
        <Paper style={styles.centerBox}>

          <div style={styles.login}>

            <div>
              <center><img style={styles.img} src="https://upload.wikimedia.org/wikipedia/en/thumb/d/d1/US_Foods_logo.svg/1105px-US_Foods_logo.svg.png" /></center>
            </div>

            <div style={styles.feilds}>
              <h1 style={styles.loginHeading}>Login</h1>
              <h3 style={styles.fieldHeading}>Email</h3>
              <TextField id="search" InputLabelProps={{
                  style: {
                    textOverflow: 'ellipsis',
                    whiteSpace: 'nowrap',
                    overflow: 'hidden',
                    width: '100%',
                    color: 'red'
                  } }}  color="red" label="abc@xyz.com" type="search"  margin="normal" style={{width:'50%',marginTop:'-5px',color:'red'}}/>

              <br/>
              <br/>
              <h3 style={styles.fieldHeading}>Password</h3>
              <TextField id="search" InputLabelProps={{
                  style: {
                    textOverflow: 'ellipsis',
                    whiteSpace: 'nowrap',
                    overflow: 'hidden',
                    width: '100%',
                    color: 'red'
                  } }}  label="Password" type="search"  margin="normal" style={{width:'50%',marginTop:'-5px'}}/>

              <br/>
              <br/>
              <Button variant="contained" color="secondary" >
                Login
              </Button>
            </div>

          </div>

          <div style={styles.signUp}>

            <h1 style={styles.signUpHeading}>Not A Member ?</h1>

            <br/>
            <br/>
            <br/>
            <h4 style={styles.signUpText}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </h4>
            <br/>
            <h4 style={styles.signUpText}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </h4>
            <br/>
            <br/>
            <br/>
            <center>
            <Button variant="outlined" style={{color:'white',borderColor:'white'}}>
              Register Now
            </Button>
          </center>
          </div>

        </Paper>
      </div>
    );
  }
}

const styles = {
  container:{
    backgroundImage:"url('https://foodrevolution.org/wp-content/uploads/2018/04/blog-featured-diabetes-20180406-1330.jpg')",
    width:window.innerWidth,
    height:window.innerHeight,
    backgroundSize:'cover',
  },
  centerBox:{
    height:(window.innerHeight/4) * 3,
    width:(window.innerWidth/4) * 3,
    backgroundColor:'white',
    position:'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    margin:'auto',
    display:'flex',
    flexDirection:'row'
  },
  login:{
    flex:2,
  },
  signUp:{
    flex:1,
    backgroundColor:"rgba(244, 66, 66,0.7)"
  },
  img:{
    height:'70px',
    margin:'10px 0px 0px 0px'
  },
  loginHeading:{
    fontFamily: 'Roboto',
    margin:'25px 0px 10px 0px',
    color:'#f44242'
  },
  feilds:{
    margin:'20px 0px 0px 60px',
  },
  signUpHeading:{
    fontFamily: 'Roboto',
    textAlign:'center',
    color:'white'
  },
  signUpText:{
    fontFamily: 'Roboto',
    color:'white',
    margin:'0px 10px 0px 10px'
  },
  fieldHeading:{
    fontFamily: 'Roboto',
    margin:'25px 0px 10px 0px',
  }


}
