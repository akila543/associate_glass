import React, { Component } from 'react';

import { Icon } from 'semantic-ui-react';
import {HashRouter, Route, Link} from 'react-router-dom';


import HeadingBar from './../components/heading.jsx';
import MobileAppbar from './../components/mobileAppbar.jsx';
import MenuHeading from './../components/menuHeading.jsx';
import MenuList from './../components/list.jsx';
import DummyList from './../components/dummyList.jsx';

var data = [{
  name:'Neil Young',
  link:'/feedBackny/0',
  city:'France',
  state:'',
  task:'90 %',
  hrs:'8.75',
  color:"#ffe200",
  img:'client/assets/images/face.jpeg'
},
{
  name:'Amit Sharma',
  link:'/feedBackas/0',
  city:'France',
  state:'',
  task:'100 %',
  hrs:'8.1',
  color:"#75e064",
  img:'client/assets/images/face1.jpg'
},
{
  name:'William Scott',
  link:'/feedBackws/0',
  city:'France',
  state:'',
  task:'50 %',
  hrs:'6',
  color:"red",
  img:'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUSEhIWFRUWFxYVFRUVFhcVFxcXFRUXFxcVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQFy0dHR0tLS0tLSstLS0rLS0tLS0tLSstKystLS0rLS0tLTctKy0tLSstLS0tLS0tLSsrNystN//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAACAQMEBQYAB//EAEAQAAEDAgMFBgMFBwIHAQAAAAEAAhEDIQQFMQYSQVFhInGBkaGxE8HwBxQyUtEzQnKCwuHxYrIWIyQ0c7PiFf/EABgBAAMBAQAAAAAAAAAAAAAAAAABAgME/8QAIBEBAQACAgMBAQEBAAAAAAAAAAECEQMhEjFBUTIiBP/aAAwDAQACEQMRAD8A15CSEZCQhNRshIQnEJCAbIQkJwhCUA2UJCcIQFANkISnCmcRVDWk8gT5BAN4qqGNLjoNTyHNQ2ZmwgkGbx+qymd7VFzXNAsWlttJmJveN0n0WUoZg9jSAeET36nvU7Np892oO85rI7Lo3uB3dD5j2WSqY0mZMz/e/qU1UMlB9XSGx1KpOqSLSmpJt7pXuJHTly8EDZwmBM/2Stv38pUdnunKbougbTviBnDviT4JDXNQ8TykmJ5BvHwuotDhPH52U01QyGzLuLuXNo5D1Qex0w4GxjWTy8fPqrbL6zt4RwNoMTzJPM9FQV8UTEAgDTXzPNWGVVom9zEnxH6FTTlbzLajniXeHmeHSw8FIc1VOVYlraUaHQ9D2QB3Tp3q3Y/eEqsaWUMuCAhPuCbcFSTDgmnBSHBNuCAZhcjhcgN8khGQhTASEJCOEhQDZCEhOEIHIBsoHJwhNuQArIbTZ0R2Kbt11x0My30NlYbUZ8yg3c3oeegJA5wvLMbjnum9pnx5weiVphxtbedItJmORNyPAyo76hd3Dgmn1CdTKRswpLZd46BKGnjKRhI4wukjQygOaClf4pA8rjy9kAsoolA1nVOtZxlAcx1/b+66u8h313I6kWPuk35tYib/AFoUHTYfz7tVe4PdZTafzOgnjYHnykeipywRYX5f5Umm7Rrid0EyRfUX5crJWbOVo8FXnctJcWkeBBj0HkVrmYgEwNBeepn9F57lWKu0A/hMDnHBW2VY+SQDJJbA1FrTH1qp9VXuNeU24I6bhpMxqeE8lzgtUGCEBCecEBCBDMLk5upEG3RCRGhTSBIUZQoAChIThCEoBpwUbFVm02ue4wAJKluCyX2hY0sw/wAMfvm+lwLxr8kB5xn2PNaq95mCTYmYVSZPFSXpoMUHTbaJ4JRTIUxjBxdJ6DTon3YcR9XKBpW7iJrVMdhDrCZYy8FA0aDE2WwVNbSAPPohfuE2t1PtZB6Rd3qnKVikgpynSM2ty4II5Vpe0x8jPFRfhnUeKlhxFiR4/wBkyafIz9fXmgU5SEt3tYtrHgUTsYI3Y8Ikd3TvSN7Otge7X5rn07T8h5oMmH4EWj8XhpBnX65qY7E7j95siWxrzs46a2Pmo0gcOXU36QgxJlw04AeQm/G8pBttnKhdEkwO13lxP6nvWkIWAy/NXMEAD5d5J7uHVbnL940w5xkm/d0hPEUZCbcE+Qm3BUDMLk7C5AbVIQiISJpBCQoikKAAoCnCgKAArzT7TMRNZjO12WzF4ve3NelleQ7dV9/F1DeAYEg8ABYQlTjNESb+icay9kjQpWHYCoM22ny4qZRoRdSsLhByU6lQUZZtceNCkDhvHnoB3FNfct64EFXdPDidFaUMECFneRpOJjKmDIBtcqsq0SNV6SMqB4Kvx2yz3HsjrPT9VWPJEZcVjBhk6/Xknm9XRwnWPLgrnE5C9tuPWfQqvqYN7Tf1/VaeUZ+Nhk0yRaJTdJhiwuOEa+l0/biDPSfdN1Kjge68+qZBoYl0w5oM2uPq6lNlokDeabFpvIOon5IKGIkw9oPXQjvKeed240sDAnu3ggQ3Wwu9oCW8DxbPCD9eqZrNs2bkCL2IuVY0Xk2LYkGC3Q8be8IalAE69ZiYMT+qBpDos3SDG8NdfkvRsjLjSaXRfSCfmAsCykAwkm1o77aea3uzbZoNJ7u6DCMRU8hAQnyEBarI1C5HupUBr0hRwhTSAoSjKEhAAUJRlAUADl4ln9f4leo7m86dLBeyZtU3aNR2kMd7dF4dWfLippw2pmEUMBS8GoyXj7XdI2UmgFHoiymYQLnrrxh6lqrXCFVjW3VngworSLbDNCsGDgq/DqbRfcIhXsxiMtDpkSCs7meRjUeS3DDIUWvh54K5Wet+3l+NyA6i6rXYRzTBHgf14L1R+XA8FFxGRB193vVzOs7hHmjsO0atg9f14hP4ciYI+v0W0rbN8AAR+U8O7l3KuqbOEAkTY+/yVTMrx/imZhN3T8JMjofr3Tr8F2SQJEajlvbv9Q8lcNwBAgiD9XVdjHljKlM6j2MG3jCfltNx0oa7+xujn43gz6eq32zTT92pyIMLz+lSJ3QV6fgqW7TY3k0ey0xZ0RCEtTsJN1UmGoXJ2FyDalIUSQppAQhKMoSEABTZTpQFAUG2jy3B1I4wD3EheOP1Xsu2g/6OrAnTw7QuvF67rqaY2NlWeDoQm8toyJKsQ1ZZVrhj9SaKnUAoeHarBjFhXTicYFZ4RiraWquMFdTVptFqfDVKwlAQpLsHZEhbM4eU+9qdoUYUg0ZVyItQmU0+Kc8E+2jCcbTQnaJ93HJNVMECHRqVafCSNalT2qcVljZJAE2jwFgsLtdgt2ADc2J/VelVeJWF2qYajw2NSnh7LL0yWU4EvqsEGJE+Gq34aq7JcGGyYvoCrQrqx9ObK9ghDCcIQqiBAXI1yA0xSJSkTSEoCjIQlACVm9usQWYaASN94aYMGIcY9FpHLHfaNUO5RbwLnOP8oAH+4pZXpWPuMb99qUqNSm071OoLtJsCDIe3kfdZWZK0dJzi0tcLcD8lSUqU1d3/AFLLGtOSLzCMhoCTEYprNSpDW2UOtl4cZKlfcnQKOdsB4q7wWcUnWJjvWfqZQ06WKaGVVB+FzT6IuONKZZxuab2m4Vjgqg5rzUYyvRPaBHfcK5yzaa/bGvFReNc5v16dgKiuaT5HyWIyzOmHR0rR4XGyFOtLva5TtJVrMYCpNDEBOVNie0JxtNRm11IZWVJpKjITbu5PIKiiiIGMeACsviKRdUnl8wtHi+az1V81YiIk+gFvVGE7PO9DYwAWSlKuXZpy7AkIRFCUDZFySEqDadCiKFNJChROQlMAKyn2gMHw6JPB7h4Fsn2C1hWU+0Fk0qf8Z9Wn9FGXpeH9RgDU3iTwaLd6rcqZNQu704CWOLXaXv4Jcj4lZz01z7sXVFkpzE0oFl2HVlSobwWVvbSTbJY1jzYIf/zewHb5Di4AkmANfnZa2plc8EVPKAbOarmURlx7ZfLMoxLqr8O6oG1A4AMeN5rw6/YcdTF0WZ5IaFQ06rd0jRzND1g+y2OBwRou3qZDTETugkDpIsq/O8M+rM1JMzJAsQDpEWuq8omcd+s1h6DqbgWukLc5PjCWgSsbiGFpA5awtjspg3OiyyzvTXCaXZqFolVdTaYMMG11os4wW5Tk6rzXOGDe7TdesJSdqt6aj/jalpKn4fbGh+eei8tzLCfBAeaZaCJHa4Dj01RYaCGl7aoB0dAcIFydNIWvhGNzu9PYqG2VD809ytcDntGsOw4TyJuvJaOVUnAFlcje0JAg9xBTr8jxNPtMqAjp7FTdH3+PVsYRuOdyBJ8llMpokN3nTLiSZ4XVhk1Sq7BOFWS4kMbzIJHE3MdryU5uAa1hL3AGCRHONIRhqUspbKrihKIoV0ucJQFGUBQCLkq5AaYpEpSJgJSFEUKAEqg22wu/hXEasIf5WPoStAUziaLXtcx2jgWnuIhKzo5dV4rmlAGmXcrqLkJ1V5tTk1bDMeHCWfuvGh7/AMp6FZ7JnQVj8a5WXKaaKibq/wAuWdoG6u8E+Fjk6MF/SphSW01EwlRWDVMXYadS6KozhzWNkC6tcTW3QsnmuK3jCrabFXTw++/xXp2x2E3QD0WEyWhLx3heqZFRDW6I90r1iHO6W8YjgsHnWU7xJLb816RVbJTNTANdchPXaJdPLfuTrAmQA5oDgHWdqCDqFabLZU2iQDLmMBDGGN0FwguJcSTYxC19fJGO6LqGSxyKuZWFZhWHx2z9RlR7qTQWPMvpD8Ik3c3kZ4BaHJ8sduAHhzv4dVqaOAjgnn0gBZRl2csnUVlTDQ1lNtol3lA/q9FTPaQ528STJ14DgFZ4jGE1y0aNZDvET81WvMmVfFN3f4nktxx1+myhKMoSuhzAKEoihKARcuXIDTFIuXJgJSJShQCFA5GU25AZD7SyfupG9AkSL9qXCBy1Ery7LnQ5ek/ajiAMOxnFzx32+gvM8M6CFGSp7aakVb4J6pcI+QrHCOgrmydONabBOVk0qmwb1atNlDdEzJ9isfVqbxlWe0WOMhnPXuUCoAGgqsYi9tDsvh95wXpmCYAyF55seQSvRaBBaiJ5PUNVRdGHKPiqkXRUasqpU66PEpym4JrfCBxTtTpN+KFVZrjhTY5x0aJPhyXVcQQs7mNb4tWnRB1Jqv6U6V797t0KPd0etTaTRpGnTJeZq1O0/juzctnxjwTBT1epvGfIdE0V1YY+MYZ5+V2AoCjKEqkAKEo0CARclXIDRJJXSuTASUhK4rkAhTbkZQOQGH+1DDg0Wvm4cBHQ/Q8l5nSbdenfag8/BpsHEuef5YA/3FeXU3KL7NaZbiYK0OGdMQsnTsJ7ldZTiZPt3BZZRrhk1+AKt2usshWzYUx1UU7UO526j2uspha3vJIk7TN7W8OCz1TFvkFrjE3abjw5Isyzn4gjmVWU3Em5jqtccemWWc309A2czPcEHitONqW0m9p2veT5BebYPMjTYLCSSJVxl2PDtzfaHSe0DwvEjr0UXFpM5eno+BzVuJp9i/WD81Lw7iE5k9BjaYDQAI4WR1KRExE8FNh+U9Hd9R61eEL3xyVdiq3VK0tGsfmFiZsLkqg2bxD6tStWOjoa3o0Tb28lUbT5mXu+DT46kLWZdg20mMa0RDQPQLbix+1ly5fIklCURQlbsAlCURQlAAUJRlCUAKVcuQF/K5CuTDikSSuKAQrH7a7WfdgadIj4nF2u5PADi72Vzj9pKDHOpNfv1g1x3W9oNj850bci2vReK7QYp1Sq6STBMnm6blTlfgW+GxL6zN+o4vc4ky4knWNT3LPVWlji08CrvLP2bR097qLneH0eO4+eqyl7a5Y/5iKx1j9aKxyp3n8uip6D1YYauGgJ1MqyxtPnPcq91CdTcmw0/wAqdgqvxXCSrmrQYRDgCFG9NJj5KEZWYER1Kl4DIpcA6qxsnjpfmrKlkFN34XEeKmU9k64gtfvA8Df1T3tpOKfVTm+zFZl7FoGo0PExzN1GbTewCRBGkjXvB8FqxlWLtDA4CI7Rt6JjHCrG7XoOtYFo3rdyW6d4vsdhNsalNrW8tYWxyTaIV2gcSNe7mF5ZimQ6N1w5SCJHVaLZdhb2pLS0zPMQLeN0spNIlu+21zHHhjSXEWWNzXaLfBDTAUHabNzUcWg9n1KpKFF1R7aYHac4ADp1ta0+qMcCyzaTY/B/Fe6sRIEtBI4nUjwjzW2a2AmMswbaNNtNujR/knqTJUkrpxmmFCUJRFCUyAhKMoSgAKEo3ICkCLly5AXO8u3lGxFdrGl73BrRcucYAHeVj80+0SiyRQYah/M7sM7/AMx8gmG2qVA0EkgACSSYAA4krzna/bsumlhSQ3R1XQnmGch117lms52oxOJtUfDPyNG63xGp8SVR1CptNs9isH/yn1D+KoSAf9Lf/qfILF410vd/Efdek5FT3KdJnJonvIk+pKxe1mWfBxDrdl532+JuPAz6LOXs6PLH9kBTcRS3mkdFT5fUiyuqdSyzvttjdxlarC1xaeBSh/6KxzjBGd8XHHjZVS1l3GGU1VhhcSW6EDp+qvsDit8Q43WTpaj3V9ltcgiJ7zaUsovDJZ0cyfSN9FZ0Nst091tfoKuxtE1GxHpzt52VRTydxdz19NfdTJGnllPT0/KtrWuFyB3wDZWjs1Y7SPfyXnOU5QDG842i06xzVrXx9OkIFzB9BPjYLPL8jTy+1L2pqMPfzVTVxO5TABuR8lTZpmhqE8It7Q5R62JJbAvwHHoPFXjgzyzdiMQQepW32MyI0x8erPxHjstP7jT1/MVH2W2UI3a+IEkCWM1DZ4nmdO5bQBXhlLdRGWNk3XJCiSFbMgISiKEoAShKJCUAJQlE5CUqCJF11yA8Wr4p7vxPc7+Jxd7piVzkChQiUtPUTzHuhCV+iA9Jwz+0PD2Tu12VfeMPvNEvp9pvUcR5eyrclqbzA7oPYLXZa+RCyvQeN0xF1a4WpZT9rcm+71yWjsVJc2OB/eb5n1VTRsirxWRAIg3Cosxy8tJcB2de5XVFycqUw4QRZKXSrj5MnSmbaq1wdfd7IuZg2SY7KnAyz/CrgC3hHotdystWNthsUA25t/bUlSMFimlvKfmsX99O6RMafQ6KTQxxaN2eHkAPryU3Bc5Gu+9bt51Hlr/fyWdzDEFzyeABNvFsKPUxzzbmJ8I/umKG858NBMzbW7uXp5ImOhllt27LouTw6iJHjot9sbs5u7taq2+rW6gCLE8zdR9nNmCHB9SLTAHpHr5BbzD0t0QFnycnyNePj13T+Cob9RjODnCe6bpqo2CRyJHkVKyCqH4xtNt9xpe/pMhg8e0f5UxiyC95H5nf7ir/AOedVnz3/WjSQpUhXQxCUJRISgBQlEUJQAlCiQlAIuXJEg8OKFKuUKK1K/RcuQG22c/ZDuHstdlOi5csaFZ9on7Gl/5P6SsI1cuQrH0lUlKCRck1hwKpzPTx+QSrk8U5+lBU+SdGh7v6Vy5bMIeq6/yhaPY79sO53slXKc/S8P6j07Cqc7RcuXG7fhj7Ov8AvcZ3UvZ6bwf4T/HU/wDY5cuXXw+nHy/3TyRcuW7MJQFcuQCFCUq5AAgcuXIDly5ckH//2Q=='
},
{
  name:'Karen Simmonds',
  link:'/feedBackks/0',
  city:'France',
  state:'',
  task:'100 %',
  hrs:'7',
  color:"#75e064",
  img:'https://cdnb.artstation.com/p/assets/images/images/001/863/575/large/irakli-nadar-artstation-da.jpg?1453903033'
}]

module.exports = () => {
  return(
    <div className='bgImage'>
      <HeadingBar />
      <MobileAppbar />
      <MenuHeading heading={'Daily Performance'} color={'#d64267'}/>
      <MenuList data={data}/>

      <div style={{position:'fixed',width:'100%',bottom:'0%',backgroundColor:'rgba(0,0,0,0.5)',display:'flex'}}>

        <div style={{flex:1}}>
          <center>
          <Link to="/"><Icon name="angle double left" size={'big'} inverted style={{margin:10}} /></Link>
        </center>
        </div>

        <div style={{flex:1}}>
          <center>
          <Icon name="search" size={'big'} inverted style={{margin:'10px'}} />
        </center>
        </div>

        <div style={{flex:1}}>
          <center>
          <Icon name="angle double right" size={'big'} inverted style={{margin:10}} />
        </center>
        </div>

      </div>
    </div>
  )
}
