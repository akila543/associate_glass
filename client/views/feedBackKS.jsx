import React, { Component } from 'react';
import { Icon, Modal,Header, Button } from 'semantic-ui-react'

import {HashRouter, Route, Link} from 'react-router-dom';

import HeadingBar from './../components/heading.jsx';
import MobileAppbar from './../components/mobileAppbar.jsx';
import MenuHeading from './../components/menuHeading.jsx';
// import Chat from './../components/chat.jsx';
const styles = {
  chatContainer:{
    margin:'10px',
    backgroundColor:'#4c4c42',
    height:'350px',
    borderRadius:'10px'
  },
  submitTo:{
    marginTop:'5px',
    color:'#fff',
    fontWeight:500
  },
  button:{
    justifyContent:'center',
    background:'yellow',
    padding:'7px',
    borderRadius:'10px',
    border:'none',
    outline:'none'
  },
  textarea:{
    margin:'13px',
    borderRadius:'10px',
    textIndent:'5px',
    border:'none',
    outline:'none'
  },
  heading:{
    marginLeft:10,
    paddingTop:10,
    color:'white',
    fontWeight:200,
  },
  name:{
    margin:'-15px 0px 10px 10px',
    color:'white',
    fontWeight:'bold',
  }
}

export default class FeedBack extends Component{
  constructor(props){
    super(props);
    this.state = {
      modal:false
    }
  }

  render(){

    var link;
     if(this.props.match.params.num == 0)
        link = '/dailyPerformance';
     else
      link = '/weeklyPerformance';

    return(
      <div className='bgImage'>
        <HeadingBar />
        <MobileAppbar />
        <MenuHeading heading={'Feedback'} color={'#2dc1c4'}/>
        <div style={styles.chatContainer}>
          <center>

          <h4 style={styles.heading}>Please provide feedback for</h4>
          <h2 style={styles.name}>Karen Simmonds</h2>

          <textarea style={styles.textarea} rows="6" cols="25">
          </textarea>

        </center>
          <center>
            <input style={styles.button} type="submit" value="SUBMIT TO" onClick={() => this.setState({modal:true})}/>
            <h5 style={styles.submitTo}>DEEPAK AGARWAL</h5>
          </center>
        </div>

        <Modal open={this.state.modal} basic size='small'>

          <Modal.Content>
            <p>
              Alex, Thanks for your feedback, We will get back to you at the earliest.
            </p>
          </Modal.Content>
          <Modal.Actions>
            <Button as={Link} to={link} color='green' inverted onClick={() => this.setState({modal:false})}>
              <Icon name='checkmark' /> OK
            </Button>
          </Modal.Actions>
        </Modal>

        <div style={{position:'absolute',width:'100%',bottom:'0%',backgroundColor:'rgba(0,0,0,0.5)'}}>
          <center><Icon name="cancel" size={'big'} inverted style={{margin:10}} /></center>
        </div>
      </div>
    )
  }
}
