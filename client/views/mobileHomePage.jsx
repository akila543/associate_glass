import React, { Component } from 'react';

import MobileAppbar from './../components/mobileAppbar.jsx';
import HeadingBar from './../components/heading.jsx';
import MenuList from './../components/menuList.jsx';

import { Icon } from 'semantic-ui-react'



module.exports = () => {

  return(
    <div className='bgImage'>
      <MobileAppbar />
      <HeadingBar/>
      <MenuList />

      <div style={{position:'fixed',width:'100%',bottom:'0%',backgroundColor:'rgba(0,0,0,0.5)'}}>
        <center><Icon name="cancel" size={'big'} inverted style={{margin:10}} /></center>
      </div>
    </div>
  );
}
