import React, { Component } from 'react';

import {HashRouter, Route, Link} from 'react-router-dom';

import { Grid, Icon, Header } from 'semantic-ui-react'

module.exports = (props) => {
  return(
    <div>
    {
      props.data.map((item,index)=>(
        <div key={index} className="outerMenuContainer" style={{marginTop:10,display:'flex',backgroundColor:'rgb(0, 0, 0,.5)'}}>
          <div style={{flex:1,margin:'15px 0px 0px 5px',display:'flex'}}>
            <div style={{width:10,height:80,backgroundColor:item.color}}>
            </div>

            <img style={{height:55,marginLeft:5,width:55,borderRadius:'50%',marginTop:10}} src={item.img} />
          </div>

          <div style={{flex:3.5}}>
              <Header as="h2" inverted style={{marginLeft:20,marginTop:5}}>{item.name}</Header>

              <hr style={{backgroundColor:'white',marginTop:-10,width:'89%'}}/>

              <div>
                <Grid columns={4}>
                  <Grid.Row style={{marginTop:-5}}>
                    <Grid.Column >
                      <span style={{fontSize:8,color:'white'}}>Location</span>
                      <br/>
                      <span style={{fontSize:13,color:'#ffe200',fontWeight:'bold'}}>{item.city}</span>
                      <br/>
                      <span style={{fontSize:14,color:'#ffe200'}}>{item.state}</span>
                    </Grid.Column>

                    {/* <Grid.Column style={{marginLeft:0}}>
                      <span style={{fontSize:8,color:'white',lineHeight:1.2}}>Clocked Hours</span>
                      <br/>
                      <span style={{fontSize:14,color:'#ffe200',fontWeight:'bold'}}>{item.hrs} hrs</span>
                    </Grid.Column> */}

                    <Grid.Column >
                      <span style={{fontSize:8,marginTop:15,padding:0,color:'white',lineHeight:1.2}}>Task Completion%</span>
                      <br/>
                      <span style={{fontSize:14,color:item.color,fontWeight:'bold'}}>{item.task}</span>
                    </Grid.Column>

                    <Grid.Column >
                      <span style={{fontSize:8,margin:0,padding:0,color:'white',lineHeight:1.2}}>Commitment Adherence%</span>
                      <br/>
                      <span style={{fontSize:14,color:item.taskColor,fontWeight:'bold'}}>{item.task}</span>
                    </Grid.Column>

                    <Grid.Column width={4} style={{backgroundColor:'black'}}>
                      <center>
                      <span style={{color:'white',fontSize:10}}>Feedback</span>
                      <br/>
                        <Link to={item.link}>
                          <Icon inverted size="large" name="chat" style={{marginTop:5}}/>
                        </Link>
                      </center>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </div>
          </div>
        </div>
      ))
    }
  </div>
  )
}
