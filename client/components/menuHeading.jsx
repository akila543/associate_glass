import React, { Component } from 'react';


module.exports = (props) => {
  return(
    <div style={{backgroundColor:props.color}}>
      <h3 style={styles.headingText}>{props.heading}</h3>
    </div>
  );
}


const styles = {
  headingBar:{
    backgroundColor:'#d64267'
  },
  headingText:{
    textAlign:'center',
    paddingTop:'7px',
    paddingBottom:'7px',
    fontWeight:'bold',
    color:'white',
    textTransform:'uppercase',
    fontFamily:'san-serif'
  }
}
