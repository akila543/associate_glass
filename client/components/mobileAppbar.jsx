import React, {Component} from 'react';

import {HashRouter, Route, Link} from 'react-router-dom';


module.exports = () => {
  return(
    <div style={styles.appbar}>
    <Link to="/">  <img style={styles.logo_one} src="./client/assets/images/US_Foods_logo.png" /></Link>
      <img style={styles.logo_two} src="./client/assets/images/wipro.png" />
    </div>
  );
}

const styles = {
  appbar:{
    padding:10
  },
  logo_one:{
    height:'45px',
  },
  logo_two:{
    height:'45px',
    float:'right'
  }
}
