import React, { Component } from 'react';


module.exports = () => {
  return(
    <div style={styles.chatContainer}>
      <center>

      <h4 style={styles.heading}>Please provide feedback for</h4>
      <h2 style={styles.name}>William Scott</h2>

      <textarea style={styles.textarea} rows="6" cols="38">
      </textarea>

    </center>
      <center>
        <input style={styles.button} type="submit" value="SUBMIT TO" />
        <h5 style={styles.submitTo}>DEEPAK AGARWAL</h5>
      </center>
    </div>
  )
}

const styles = {
  chatContainer:{
    margin:'10px',
    backgroundColor:'#4c4c42',
    height:'350px',
    borderRadius:'10px'
  },
  submitTo:{
    marginTop:'5px',
    color:'#fff',
    fontWeight:500
  },
  button:{
    justifyContent:'center',
    background:'yellow',
    padding:'7px',
    borderRadius:'10px',
    border:'none',
    outline:'none'
  },
  textarea:{
    margin:'13px',
    borderRadius:'10px',
    textIndent:'5px',
    border:'none',
    outline:'none'
  },
  heading:{
    marginLeft:10,
    paddingTop:10,
    color:'white',
    fontWeight:200,
  },
  name:{
    margin:'-15px 0px 10px 10px',
    color:'white',
    fontWeight:'bold',
  }
}
