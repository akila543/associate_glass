import React, { Component } from 'react';


module.exports = () => {
  return(
    <div style={styles.headingBar}>
      <h1 style={styles.headingText}>GEFCO</h1>
    </div>
  );
}


const styles = {
  headingBar:{
    backgroundColor:'#FDCD07'
  },
  headingText:{
    textAlign:'center',
    margin:0,
    padding:0,
    paddingTop:'5px',
    paddingBottom:'5px',
    fontWeight:500,
    color:'black',
    textTransform:'uppercase',
    fontFamily:'san-serif'
  }
}
