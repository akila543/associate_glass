import React, { Component } from 'react';

import {HashRouter, Route, Link} from 'react-router-dom';

import { Grid, Icon, Header } from 'semantic-ui-react'

module.exports = () => {
  return(
    <div className="outerMenuContainer" style={{marginTop:50}}>
      <Grid style={{margin:'20px 0px'}}>
        <Grid.Row>
          <Grid.Column width={1} />

          <Grid.Column width={3} style={{backgroundColor:'grey',padding:15}}>
            <center><Icon inverted name="line graph" size="big" /></center>
          </Grid.Column>
          <Grid.Column width={11} style={{justifyContent:'center',backgroundColor:'#d64267',borderTopRightRadius:'10px',borderBottomRightRadius:'10px'}}>
            <Link to="/dailyPerformance"><Header as='h3' style={{fontWeight:800,color:'white',padding:16}}>Daily Performance</Header></Link>
          </Grid.Column>

          <Grid.Column width={1} />
        </Grid.Row>

        <Grid.Row>
          <Grid.Column width={1} />

          <Grid.Column width={3} style={{backgroundColor:'grey',padding:16}}>
            <center><Icon inverted name="chat" size="big" /></center>
          </Grid.Column>
          <Grid.Column width={11} style={{justifyContent:'center',backgroundColor:'#2dc1c4',borderTopRightRadius:'10px',borderBottomRightRadius:'10px'}}>
            <Link to="/feedBack"><Header as='h3' style={{fontWeight:800,color:'white',padding:16}}>Feedback</Header></Link>
          </Grid.Column>

          <Grid.Column width={1} />
        </Grid.Row>

        <Grid.Row>
          <Grid.Column width={1} />

          <Grid.Column width={3} style={{backgroundColor:'grey',padding:15}}>
            <center><Icon inverted name="setting" size="big" /></center>
          </Grid.Column>
          <Grid.Column width={11} style={{justifyContent:'center',backgroundColor:'#f4d442',borderTopRightRadius:'10px',borderBottomRightRadius:'10px'}}>
            <Link to="/weeklyPerformance"><Header as='h3' style={{fontWeight:800,color:'white',padding:16}}>Weekly Dashboard</Header></Link>
          </Grid.Column>

          <Grid.Column width={1} />
        </Grid.Row>

      </Grid>

    </div>
  )
}

const styles = {
  icon:{
    height:'50px',
    marginTop:'25px',
  },
  heading:{
    fontWeight:500,
    padding:'10px',
    backgroundColor:'#d64267',
    textAlign:'center',
    marginLeft:'20px',
  }
}
