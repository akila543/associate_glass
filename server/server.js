const express = require('express')
    , app = express()
    , server = require('http').Server(app);

app.use(express.static('./../'));
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
     next();
});
server.listen(5555, function() {
    console.log('server started on  5555');
});
