import React from 'react';
import ReactDOM from 'react-dom';

import {HashRouter, Route, Link} from 'react-router-dom';

import Calendar from './client/views/calendar.jsx'
import LoginPage from './client/views/login.jsx'

import MobileHomePage from './client/views/mobileHomePage.jsx';
import DailyPerformance from './client/views/dailyPerformance.jsx';
import WeeklyPerformance from './client/views/weeklyPerformance.jsx';
import FeedBackAS from './client/views/feedBackAS.jsx';
import FeedBackKS from './client/views/feedBackKS.jsx';
import FeedBackNY from './client/views/feedBackNS.jsx';
import FeedBackWS from './client/views/feedBackWS.jsx';
import FeedBack from './client/views/feedback.jsx';


ReactDOM.render(<HashRouter>
                 <div>
                    <Route exact path='/' component={MobileHomePage} />
                    <Route  path='/dailyPerformance' component={DailyPerformance} />
                    <Route  path='/weeklyPerformance' component={WeeklyPerformance} />
                    <Route  path='/feedback' component={FeedBack} />
                    <Route  path='/feedBackas/:num' component={FeedBackAS} />
                    <Route  path='/feedBackks/:num' component={FeedBackKS} />
                    <Route  path='/feedBackny/:num' component={FeedBackNY} />
                    <Route  path='/feedBackws/:num' component={FeedBackWS} />
                 </div>
                </HashRouter>, document.getElementById('app'));
